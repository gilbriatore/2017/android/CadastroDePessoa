package com.cadastrodepessoa;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.cadastrodepessoa.dominio.Pessoa;
import com.cadastrodepessoa.persistencia.CadastroDePessoa;

public class TelaPrincipal extends Activity {

	private EditText txtNome;
	private EditText txtSobrenome;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tela_principal);

		txtNome = (EditText) findViewById(R.id.txtNome);
		txtSobrenome = (EditText) findViewById(R.id.txtSobrenome);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_tela_principal, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent = new Intent(this, TelaDeCadastro.class);
		startActivity(intent);
		return true;
	}

	public void buscar(View v) {

		CadastroDePessoa cadastro = new CadastroDePessoa(this);

		String nome = txtNome.getText().toString();
		String sobrenome = txtSobrenome.getText().toString();

		ArrayList<Pessoa> pessoas = cadastro.listarPessoas(nome, sobrenome);
		
		if (pessoas.size() == 0){
			Toast.makeText(this, "Nenhuma pessoa cadastrada.", Toast.LENGTH_SHORT).show();
		} else {
			Intent intent = new Intent(this, TelaDeListagem.class);
			intent.putExtra("pessoas", pessoas);
			startActivity(intent);
		}

	}
}
