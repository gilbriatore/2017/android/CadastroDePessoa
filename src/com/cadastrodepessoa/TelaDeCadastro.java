package com.cadastrodepessoa;

import java.io.Serializable;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.cadastrodepessoa.dominio.Pessoa;
import com.cadastrodepessoa.persistencia.CadastroDePessoa;

public class TelaDeCadastro extends Activity {

	private EditText txtNome;
	private EditText txtSobrenome;
	private Pessoa pessoa;
	private int position;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tela_de_cadastro);

		txtNome = (EditText) findViewById(R.id.txtNome);
		txtSobrenome = (EditText) findViewById(R.id.txtSobrenome);

		Serializable s = getIntent().getSerializableExtra("pessoa");

		if (s != null) {
			pessoa = (Pessoa) getIntent().getSerializableExtra("pessoa");
			position = getIntent().getIntExtra("position", 0);

			txtNome.setText(pessoa.getNome());
			txtSobrenome.setText(pessoa.getSobrenome());
		}
	}

	public void gravar(View v) {

		if (txtNome.length() > 0 && txtSobrenome.length() > 0) {

			Pessoa pessoa = null;
			if (this.pessoa != null) {
				pessoa = this.pessoa;
			} else {
				pessoa = new Pessoa();
			}

			pessoa.setNome(txtNome.getText().toString());
			pessoa.setSobrenome(txtSobrenome.getText().toString());

			CadastroDePessoa cadastro = new CadastroDePessoa(this);

			if (pessoa.getId() > 0) {
				cadastro.alterar(pessoa);
			} else {
				cadastro.incluir(pessoa);
			}

			txtNome.setText("");
			txtSobrenome.setText("");

			Toast.makeText(this, "Cadastro realizado com sucesso.",
					Toast.LENGTH_SHORT).show();

		} else {
			Toast.makeText(this, "Nome e sobrenome são obrigatórios.",
					Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();

		Intent intent = getIntent();
		intent.putExtra("pessoa", pessoa);
		intent.putExtra("position", position);
		setResult(TelaDeListagem.TELA_ALTERAR, intent);

	}

}
