package com.cadastrodepessoa.persistencia;

import com.cadastrodepessoa.dominio.Pessoa;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BancoHelper extends SQLiteOpenHelper {
	
	private static final String NOME_DO_BANCO = "cadastro.db";
	private static final int VERSAO = 1;
	

	public BancoHelper(Context context) {
		super(context, NOME_DO_BANCO, null, VERSAO);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		StringBuilder sql = new StringBuilder();
		sql.append("CREATE TABLE IF NOT EXISTS ");
		sql.append(Pessoa.PESSOA + " ( ");
		sql.append(Pessoa.ID + " integer primary key autoincrement, ");
		sql.append(Pessoa.NOME + " text, ");
		sql.append(Pessoa.SOBRENOME + " text); ");
		
		db.execSQL(sql.toString());
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		//Não utilizado ainda.
	}

}
